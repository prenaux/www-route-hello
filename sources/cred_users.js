/* global __BACKEND__ */
var NI = require('tkjs-ni');
var MODEL = require("tkjs-model");
var API_USERS = require('./api_users.js');

var credHandlerUsers = {
  protocol: "cred_users",

  model: MODEL.struct({
    user_name: API_USERS.typeUserName.model,
    user_pwd: MODEL.String
  }, 'modelCreadHandlerUsers'),

  options: {
    fields: {
      user_name: {
        label: "Name",
        error: function () {
          return API_USERS.typeUserName.errorMessage
        }
      },
      user_pwd: {
        label: "Password",
        type: 'password'
      }
    }
  },
};
module.exports = credHandlerUsers;

//----------------------------------------------------------------------
// Server
//----------------------------------------------------------------------
if (__BACKEND__) { (function() {
  function login(aPayload,aOnResult) {
    var userName = aPayload.user_name.toLowerCase();
    var userPwd = aPayload.user_pwd;
    NI.log("Cred Users trying login: " + userName);

    API_USERS.getUserInfo(userName, userPwd).then(function(aUserInfo) {
      if (aUserInfo.error || NI.isEmpty(aUserInfo.user_name)) {
        aOnResult(aUserInfo.error || "Login failed.");
      }
      else {
        aOnResult(undefined, {
          name: aUserInfo.user_name,
          email: aUserInfo.user_email,
          acl: NI.shallowClone({ regular: true }, aUserInfo.user_acl)
        });
      }
    }).catch(function(aError) {
      aOnResult(NI.formatError(aError));
    });
  }
  credHandlerUsers.login = login;
})(); }
