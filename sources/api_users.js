/* global __BACKEND__ */
var NI = require('tkjs-ni'); // eslint-disable-line
var MODEL = require("tkjs-model");

//==========================================================================
// Models
//==========================================================================
exports.typeUserName = {
  errorMessage: 'can only contain letters, digits and _ and must be between 2 and 30 characters long.',
  model: MODEL.subtype(MODEL.String, function(aStr) {
    return aStr && aStr.length > 0 && /^[a-z0-9_]{2,30}$/i.test(aStr);
  })
};

MODEL.registerModel("users:get_info", {
  userName: MODEL.String
});

//----------------------------------------------------------------------
// Server
//----------------------------------------------------------------------
if (__BACKEND__) { (function() {
  var ROUTE = require('tkjs-route');

  var users = {
    "fagiano": {
      user_name: "fagiano",
      user_pwd: "abc123",
      user_email: "fagiano@lvh.me",
      user_acl: {}
    },
    "ninja": {
      user_name: "ninja",
      user_pwd: "foo123",
      user_email: "ninja@lvh.me",
      user_acl: { admin: true }
    },
  };

  function getUserInfo(aUserName,aUserPwd) {
    return NI.Promise(function(aResolved,aRejected) {
      var user = users[aUserName];
      if (!user) {
        aRejected("Couldn't find user '" + aUserName + "'");
      }
      else {
        if (aUserPwd && aUserPwd != user.user_pwd) {
          aRejected("Invalid password for user '" + aUserName + "'");
        }
        else {
          aResolved(NI.deepClone(user));
        }
      }
    });
  }
  exports.getUserInfo = getUserInfo;

  //== getUserInfo =====================================================
  ROUTE.API({
    acl: false,
    model: "users:get_info",
    get: "/api/1/users/get_info",
    onPromise: function(aReq,aRes,aPayload,aSession) {
      return getUserInfo(aPayload.userName,null).then(function(aUserInfo) {
        aUserInfo.is_me = ROUTE.ACL.checkIsUser(aPayload.userName, aSession);
        if (aUserInfo.is_me) {
          return aUserInfo;
        }
        else {
          // Not the user that is authenticated, return only partial info
          return {
            is_me: aUserInfo.is_me,
            user_name: aUserInfo.user_name,
          }
        }
      });
    }
  });

})(); }
