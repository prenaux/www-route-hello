require('./app_config');
var NI = require('tkjs-ni');
var ROUTE = require("tkjs-route");

ROUTE.startServer(function(/*aROUTE*/) {
  var startTime;
  var times = {};

  startTime = NI.timerInSeconds();
  {
    // Initialize MOMENT and the timezone to use
    var MOMENT = require('moment-timezone');
    MOMENT.tz.setDefault(global.TZ);
  }
  times["base"] = NI.timerInSeconds()-startTime;

  startTime = NI.timerInSeconds();
  {
    require('./api_users');
    require('./api_auth');
    require('./api_hello');
  }
  times["API"] = NI.timerInSeconds()-startTime;

  startTime = NI.timerInSeconds();
  {
    require('./app_routes');
  }
  times["app_routes"] = NI.timerInSeconds()-startTime;

  startTime = NI.timerInSeconds();
  {
    require('tkjs-route/sources/components')
      .initReactRenderer(require('react'),undefined,require('react-dom/server'))
      .registerComponents(require('./components_common'))
      .registerComponentsRoutes(true)
    ;
  }
  times["components"] = NI.timerInSeconds()-startTime;

  NI.log("Startup times: %K", times);
});
