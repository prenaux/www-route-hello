var ROUTE = require("tkjs-route");

// ACL
ROUTE.ACL.registerACL([
  { url: "/favicon.png?", acl: "__public__" },
  { url: "/fonts/", acl: "__public__" },
  { url: "/img/", acl: "__public__" },
]);

ROUTE.GET_PAGE('/','home.html.ejs');

// Admin pages
ROUTE.ACL.registerACL([
  { url: "/admin/", acl: "admin" }
])
ROUTE.GET_PAGE('/admin/index', 'admin_home.html.ejs');
