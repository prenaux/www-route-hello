var React = require('react');
var R = React.createElement;
var Alert = require('react-bootstrap/lib/Alert');
var ReactCSSTransitionGroup = require('react-addons-css-transition-group');

var DismissableMessage = React.createClass({
  propTypes: {
    type: React.PropTypes.string,
    className: React.PropTypes.string,
    msg: React.PropTypes.node
  },

  getDefaultProps: function() {
    return {
      type: 'info',
      msg: 'An alert message',
      className: ''
    }
  },

  getInitialState: function() {
    return {
      alertVisible: true
    };
  },

  render: function() {
    var that = this;
    return R(
      ReactCSSTransitionGroup, {
        transitionName: that.props.className+"-transition",
        transitionAppear: true,
        transitionAppearTimeout: 500,
        transitionEnterTimeout: 500,
        transitionLeaveTimeout: 300
      },
      (!that.state.alertVisible) ? "" :
      R(
        Alert, {
          key: 0,
          className: that.props.className,
          bsStyle: that.props.type,
          onDismiss: that.handleAlertDismiss
        },
        that.props.msg
      )
    );
  },

  handleAlertDismiss: function() {
    var that = this;
    that.setState({alertVisible: false});
  },

  handleAlertShow: function() {
    var that = this;
    that.setState({alertVisible: true});
  }
});

module.exports = DismissableMessage;
