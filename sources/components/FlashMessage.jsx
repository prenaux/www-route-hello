var NI = require('tkjs-ni');
var React = require('react');
var R = React.createElement;
var DismissableMessage = require('./DismissableMessage');

var FlashMessage = React.createClass({
  statics: {
    className: 'FlashMessage'
  },

  propTypes: {
    flash: React.PropTypes.object,
    isMain: React.PropTypes.bool
  },

  getInitialState: function() {
    return {
      flash: this.props.flash
    }
  },

  getDefaultProps: function() {
    return {
      flash: { info: ["default","messages"] }
    }
  },

  componentWillMount: function() {
    if (typeof window !== undefined && this.props.isMain) {
      var that = this;

      window.flashMessage = function(aType, aMessage) {
        return that.flashMessage(aType,aMessage);
      }

      window.clearMessage = function(aType) {
        return that.clearMessage(aType);
      }

      window.replaceMessage = function(aType, aMessage) {
        return that.replaceMessage(aType, aMessage);
      }
    }
  },

  clearMessage: function(aType) {
    var flash = NI.shallowClone(this.state.flash);

    if(flash[aType]) {
      flash[aType] = undefined;

      this.setState({
        flash: flash
      });
    } else if(aType === 'all') {
      this.setState({
        flash: {}
      });
    }
  },

  replaceMessage: function(aType, aMessage) {
    var flash = NI.shallowClone(this.state.flash);
    flash[aType] = [aMessage];

    this.setState({
      flash: flash
    });
  },

  flashMessage: function(aType, aMessage) {
    // NI.log("... flashMessage: %s: %s", aType, aMessage);
    var flash = NI.shallowClone(this.state.flash);
    if (!flash[aType]) {
      flash[aType] = [];
    }
    flash[aType].push(aMessage);
    this.setState({
      flash: flash
    })
  },

  render: function() {
    var flash = this.state.flash;
    var infos = NI.selectn("info",flash);
    var errors = NI.selectn("error",flash);
    var warnings = NI.selectn("warning",flash);
    return R(
      "div",{},
      NI.map(errors, function(msg,index) {
        return R(DismissableMessage, { type: "danger", className: 'flash-message', key: index, msg: msg });
      }),
      NI.map(warnings, function(msg,index) {
        return R(DismissableMessage, { type: "warning", className: 'flash-message', key: index, msg: msg });
      }),
      NI.map(infos, function(msg,index) {
        return R(DismissableMessage, { className: 'flash-message', key: index, msg: msg });
      })
    );
  },
});

module.exports = FlashMessage;
