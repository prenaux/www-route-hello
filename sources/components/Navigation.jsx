var NI = require('tkjs-ni');
var React = require('react');
var R = React.createElement;
var Navbar = require('react-bootstrap/lib/Navbar');
var Nav = require('react-bootstrap/lib/Nav');
var NavItem = require('react-bootstrap/lib/NavItem');
var NavDropdown = require('react-bootstrap/lib/NavDropdown');
var NavHeader = require('react-bootstrap/lib/NavbarHeader');
var NavBrand = require('react-bootstrap/lib/NavbarBrand');
var NavCollapse = require('react-bootstrap/lib/NavbarCollapse');
var NavToggle = require('react-bootstrap/lib/NavbarToggle');
var MenuItem = require('react-bootstrap/lib/MenuItem');

var Navigation = React.createClass({

  statics: {
    className: 'Navigation',
  },

  propTypes: {
    title: React.PropTypes.string,
    sessionUser: React.PropTypes.string,
  },

  getDefaultProps: function() {
    return {
      title: "TITLE"
    }
  },

  render: function() {
    var navUser;
    var sessionUser = this.props.sessionUser;
    if (!NI.isEmpty(sessionUser)) {
      navUser = R(
        NavDropdown, { id: "nav_menu", eventKey: 3, title: "[" + this.props.sessionUser + "]" },
        R(MenuItem, {eventKey: 1, href: "/auth/logout"}, "Log out")
      );
    }
    else {
      navUser = R(NavItem, {eventKey: 2, href: "/auth/login"}, "Log in");
    }

    var mainCount = 1;
    function getNavMain(aName,aText,aHref) {
      var href = aHref ? aHref : ("/" + aName);
      var className = "navbar-main-item-"+aName;
      if (NI.selectn("location.pathname",window) == href) {
        className += " active";
      }
      return R(NavItem,
               {
                 eventKey: mainCount++,
                 className: className,
                 href: href,
               },
               aText)
    }

    // The Navbar root element should be kept in sync with tpl_header.html.ejs
    var nav = R(
      Navbar,
      {
        inverse: true,
        fixedTop: false,
        toggleNavKey: 0,
      },
      R(NavHeader, {},
        R(NavBrand, {},
          R("a", { href: '/' }
            , R("img", { className: 'navbar-brand-logo', src: '/img/logo.png' })
           ),
          R("span", {}, this.props.title || "")
         ),
        R(NavToggle)
       ),
      R(NavCollapse, {},
        // left part
        R(Nav, { eventKey: 0, className: 'navbar-main-items-container' },
          getNavMain("home", "home", "/"),
          getNavMain("hello", "hello", "/c/Hello")
         ),
        // right part
        R(Nav, { bsStyle: 'pills', className: 'pull-right' },
          navUser)
       )
    );
    return nav;
  },
});

module.exports = Navigation;
