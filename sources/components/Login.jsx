var React = require('react');
var R = React.createElement;
var NI = require('tkjs-ni');
var Form = require("tkjs-model/sources/Form");
var CRED = require('../cred');

var Login = React.createClass({
  statics: {
    className: 'Login',
    routes: [
      { url: '/auth/login?', acl: false }
    ],
  },

  getInitialState: function() {
    return {
      kind: 'cred_users'
    }
  },

  componentWillMount: function() {
    var flash = NI.getQueryParamByName("flash");
    if (!NI.isEmpty(flash)) {
      NI.global.flashMessage("info", flash);
    }
  },

  onChange: function() {
    var that = this;
    // validates
    that.refs.login_form.getValue();
  },

  onSubmit: function(e) {
    var that = this;
    var value = that.refs.login_form.getValue();
    if (value) {
      value = NI.assignClone(
        { ctype: that.state.kind }, value
      );
    }
    else {
      NI.log("Invalid form values.");
      e.preventDefault();
    }
  },

  render: function() {
    var that = this;
    var credHandler = CRED.getCredentialHandler(that.state.kind);
    return R(
      "div",{},
      R(
        "form",
        {
          method: 'post',
          action: '/api/1/auth/login',
          id: 'login_form',
          onSubmit: that.onSubmit
        },
        R(Form, {
          ref: 'login_form',
          onChange: that.onChange,
          type: credHandler.model,
          options: credHandler.options
        }),
        R("input", { type: 'hidden', name: 'ctype', value: that.state.kind }),
        R("button", {
          className: "btn btn-info pull-right"
        }, "Log in")
      )
    );
  },
});

module.exports = Login;
