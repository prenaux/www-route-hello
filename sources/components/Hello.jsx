var NI = require('tkjs-ni');
var React = require('react');
var ReactDOM = require('react-dom');
var R = React.createElement;
var Button = require('react-bootstrap/lib/Button');

var Hello = React.createClass({

  statics: {
    className: 'Hello',
    routes: [
      { acl: "__public__" },
    ]
  },

  propTypes: {
    name: React.PropTypes.string.isRequired
  },

  getDefaultProps: function() {
    return {
      name: "NoName"
    }
  },

  getInitialState: function() {
    var that = this;
    return {
      name: that.props.name
    };
  },

  handleChange: function() {
    var that = this;
    that.setState({
      name: ReactDOM.findDOMNode(that.refs.inputname).value
    });
  },

  render: function() {
    var that = this;
    return R("div",{},
             R("h1", {}, NI.format("Hello %s !", that.state.name)),
             R("div",{},
               "Name: ",
               R("input", {
                 type: "text",
                 onChange: that.handleChange,
                 ref: "inputname",
                 value: that.state.name
               })),
             R(Button,{},"Hello"));
  },
});

module.exports = Hello;
