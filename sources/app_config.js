/* global __BACKEND__ */
var NI = require('tkjs-ni');
if (!__BACKEND__) {
  throw NI.Error("Can't be used client side.");
}
var ROUTE = require("tkjs-route");

module.exports = ROUTE.initConfig(function() {
  NI.assert.isDefined(global.isProduction);
  return {
    rootDir: __dirname + "/../",
    timeZone: 'UTC',
    appName: "Hello Route",
    appCopyright: "© 2017 Hello Route Co. All rights reserved.",
    domain: "route.com",
    cookieName: "hello-route-cookie",
    cookieDomain: global.isProduction ? "hello-route.com" : "lvh.me",
    sessionSecret: "3FECC3E1-11FA-4447-8CDC-7E549F0A5B32",
    sessionDuration: 60*24*60*60*1000,
    devPort: 1488,
    redirectAllToHttps: false
  }
});
require.extensions['.jsx'] = require.extensions['.js'];
