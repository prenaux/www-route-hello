/* global __BACKEND__ */
var NI = require('tkjs-ni');
var ROUTE = require("tkjs-route");
var MODEL = require("tkjs-model");
var CRED = require('./cred');

//==========================================================================
// Basic models
//==========================================================================
var modelLogin = MODEL.struct({
  ctype: MODEL.String,
  redirectTo: MODEL.maybe(MODEL.String)
});
exports.modelLogin = modelLogin;

//==========================================================================
// Routes
//==========================================================================
if (__BACKEND__) { (function() {

  ROUTE.ACL.addRouteStartsWith([
    { name: "/api/1/auth/", canUse: ROUTE.ACL.canUse.public },
  ]);

  function setSessionUser(req, aUserName, aUserEmail, aUserAcl) {
    var session = ROUTE.getSession(req);
    session.user_name = aUserName;
    session.user_email = aUserEmail;
    session.user_acl = aUserAcl || (aUserName ? {} : undefined);
  }

  function login(req,res,payload) {
    var credHandler = CRED.getAndValidateCredentialHandler(payload).credHandler;
    credHandler.login(payload, function(aError, aUser) {
      if (aError) {
        NI.error("User login failed: %j.", aError);
        ROUTE.flash(req, "error", aError);
        res.redirect("back");
      }
      else {
        NI.log("User login succeeded: %j.", aUser);
        setSessionUser(req, aUser.name, aUser.email, aUser.acl);
        ROUTE.bounceToRedirect(req, res, NI.format("You've logged in as '%s'.", aUser.name));
      }
    });
  }
  exports.login = login;

  ROUTE.POST("/api/1/auth/login", function(req,res) {
    var payload = MODEL.assertValidate(
      ROUTE.getRequestPayload(req),modelLogin);
    login(req,res,payload);
  });

  ROUTE.ACL.registerACL("/auth/logout", true);
  ROUTE.GET("/auth/logout", function(req,res) {
    setSessionUser(req);
    // ROUTE.setBounceToReq(req, "/");
    ROUTE.bounceToRedirect(req, res, "You've logged out.");
  });

})(); }
