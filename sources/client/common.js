/* globals $ NI */
require('../css/main.less');
var jQuery = require('jquery');
var MOMENT = require('tkjs-ni/sources/moment');
var React = require('react');
var ReactDOM = require('react-dom');

window.NI = window.NI || {};
window.jQuery = jQuery;
window.$ = jQuery;
window.MOMENT = MOMENT;
window.React = React;
window.ReactDOM = ReactDOM;

var COMPONENTS = require('tkjs-route/sources/components')
    .initReactRenderer(window.React,window.ReactDOM)
    .registerComponents(require('../components_common'));
window.renderReactComponent = COMPONENTS.renderToElement;

(function() {

  var b = NI.browser = { caps: "" };

  var ua = navigator.userAgent;
  b.ua = navigator.userAgent;

  if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
    var ffversion = new Number(RegExp.$1) ;
    b.caps += "Firefox " + ffversion;
  }
  else if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
    var ieversion = new Number(RegExp.$1);
    b.caps += "IE " + ieversion;
  }
  else if (/Chrome[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
    var chromeversion = new Number(RegExp.$1);
    // capture x.x portion and store as a number
    b.caps += "Chrome " + chromeversion;
  }
  else if (/Opera[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
    var oprversion = new Number(RegExp.$1)
    b.caps += "Opera " + oprversion;
  }
  else if (/Safari[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
    var safariversion = new Number(RegExp.$1);
    b.caps += "Safari " + safariversion;
  }
  else {
    b.caps += "Unknown";
  }
  b.caps += " ";

  b.isIPhone = !!ua.match(/iPhone/i);
  if (b.isIPhone) { b.caps += "IPhone "; }
  b.isIPad = !!ua.match(/iPad/i);
  if (b.isIPad) { b.caps += "IPad "; }
  b.isIPod = !!ua.match(/iPod/i);
  if (b.isIPod) { b.caps += "IPod "; }
  b.isIOS = b.isIPhone || b.isIPad || b.isIPod;
  if (b.isIOS) {
    var iosVer = b.iosVersion = (function() {
      if (/iP(hone|od|ad)/.test(navigator.platform)) {
        // supports iOS 2.0 and later: <http://bit.ly/TJjs1V>
        var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
        return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
      }
      return [0,0,0];
    })();
    b.caps += NI.format("IOS %s.%s.%d ", iosVer[0], iosVer[1], iosVer[2]);
  }

  b.isAndroid = !!ua.match(/Android/i);
  if (b.isAndroid) {
    b.caps += "Android ";
    b.isAndroidMobile = b.isAndroid && !!ua.match(/Mobile/i);
    b.isAndroidTablet = b.isAndroid && !b.isAndroidMobile;
  }

  b.isBlackberry = !!ua.match(/BlackBerry/i);
  if (b.isBlackberry) { b.caps += "Blackberry "; }

  b.isOperaMini = !!ua.match(/Opera Mini/i);
  if (b.isOperaMini) { b.caps += "OperaMini "; }
  b.isIEMobile = !!ua.match(/IEMobile/i);
  if (b.isIEMobile) { b.caps += "IEMobile "; }

  b.isBot = !!ua.match(/googlebot|yahoo|msnbot|bingbot|slurp|teoma/i);
  if (b.isBot) { b.caps += "Bot "; }

  b.isTouch = ('ontouchend' in document) || navigator.maxTouchPoints > 0 || navigator.msMaxTouchPoints > 0;
  if (b.isTouch) { b.caps += "Touch "; }

  b.isPhone = b.isAndroidMobile || b.isBlackberry || b.isIPhone || b.isIPod || b.isOperaMini || b.isIEMobile;
  if (b.isPhone) { b.caps += "Phone "; }

  b.isTablet = b.isAndroidTablet || b.isIPad;
  if (b.isTablet) { b.caps += "Tablet "; }

  b.isMobile = b.isPhone || b.isTablet;
  if (b.isMobile) {
    b.caps += "Mobile ";
  }
  else {
    b.isDesktop = true;
    b.caps += "Desktop ";
  }

  b.isWebView = (
      /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(ua) ||
      /.*IOSWebView.*/i.test(ua) ||
      /.*AndroidWebView.*/i.test(ua)
  );
  if (b.isWebView) { b.caps += "WebView "; }

  b.caps = NI.stringTrim(b.caps).toLowerCase();
})();

window.computeWindowHeight = function() {
  return window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
}

window.computeWindowWidth = function() {
  return window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
}

window.PopupCenter = function(url, title, w, h) {
  // Fixes dual-screen position                         Most browsers      Firefox
  var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
  var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

  var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
  var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

  var left = ((width / 2) - (w / 2)) + dualScreenLeft;
  var top = ((height / 2) - (h / 2)) + dualScreenTop;
  var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

  // Puts focus on the newWindow
  if (window.focus) {
    newWindow.focus();
  }
}

window.getFormValues = function(aFormId) {
  var values = {};
  $.each($(aFormId).serializeArray(), function(i, field) {
    values[field.name] = field.value;
  });
  return values;
}

window.jsonParseArray = function (astrJson) {
  if (!astrJson || !astrJson["length"] || astrJson.length == 0) {
    return [];
  }
  try {
    return JSON.parse(astrJson);
  }
  catch (e) {
    NI.error("NI.jsonParseArray: " + e);
    return [];
  }
};

window.jsonParseObject = function (astrJson) {
  if (!astrJson || !astrJson["length"] || astrJson.length == 0) {
    return {};
  }
  try {
    return JSON.parse(astrJson);
  }
  catch (e) {
    NI.error("NI.jsonParseObject: " + e);
    return {};
  }
};

window.jsonToString = function (aJson) {
  try {
    return JSON.stringify(aJson);
  }
  catch(e) {
    NI.log("E/jsonToString: " + e);
    return "";
  }
};

window.postToUrl = function postToUrl(path, params, method) {
  method = method || "post"; // Set method to post by default if not specified.

  // The rest of this code assumes you are not using a library.
  // It can be made less wordy if you use one.
  var form = document.createElement("form");
  form.setAttribute("method", method);
  form.setAttribute("action", path);

  for(var key in params) {
    if(params.hasOwnProperty(key)) {
      var hiddenField = document.createElement("input");
      hiddenField.setAttribute("type", "hidden");
      hiddenField.setAttribute("name", key);
      hiddenField.setAttribute("value", params[key]);

      form.appendChild(hiddenField);
    }
  }

  document.body.appendChild(form);
  form.submit();
}

window.checkUserACL = require('tkjs-route/sources/utils').checkUserACL;

window.checkSessionACL = function checkSessionACL(aRequiredAC) {
  if (!aRequiredAC) {
    return (window.SESSION_DATA) && (window.SESSION_DATA.user_acl != undefined);
  }
  return window.checkUserACL(window.SESSION_DATA.user_acl, aRequiredAC);
};
