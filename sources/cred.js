var NI = require('tkjs-ni');
var MODEL = require("tkjs-model");

var credHandlers = {}
exports.credHandlers = credHandlers;

function getCredentialHandler(aType) {
  var credHandler = credHandlers[aType]
  if (!credHandler) {
    throw NI.Error("Invalid credential type '%s'", aType);
  }
  return credHandler
}
exports.getCredentialHandler = getCredentialHandler;

function getAndValidateCredentialHandler(aPayload) {
  var credType = aPayload.ctype;
  var credHandler = getCredentialHandler(credType)
  MODEL.assertValidate(aPayload,credHandler.model);
  return {
    credType: credType,
    credHandler: credHandler
  }
}
exports.getAndValidateCredentialHandler = getAndValidateCredentialHandler;

// Register the build-in handlers
credHandlers.cred_users = require('./cred_users');
