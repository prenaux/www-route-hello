/* global __BACKEND__ */
var NI = require('tkjs-ni'); // eslint-disable-line

//----------------------------------------------------------------------
// Server
//----------------------------------------------------------------------
if (__BACKEND__) { (function() {
  var ROUTE = require("tkjs-route");
  ROUTE.ACL.addRouteStartsWith([
    { name: "/api/1/hello/", canUse: ROUTE.ACL.canUse.public },
  ]);

  ROUTE.GET("/api/1/hello/world", function(req,res) {
    try {
      var payload = ROUTE.getRequestPayload(req);
      ROUTE.OK(res, {
        __server_id: ROUTE.getAppConfig().serverId,
        payload: payload || "no params",
        foo: "bar",
      })
    }
    catch (e) {
      ROUTE.handleError(req, res, e, true);
    }
  });

})(); }
